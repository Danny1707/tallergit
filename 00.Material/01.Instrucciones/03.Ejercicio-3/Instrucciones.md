# Ejercicio 3
Comandos que utilizaremos: [Manual](Manual.md)

## Trabajemos con ramas
Imaginemos somos un desarrollador que necesita crear un nuevo feature en la recien creada clase ```SayayinDTO.java```. El cambio es sencillo, se requiere cambiar el valor de la constante ```OVER_POWERED``` de 9000 a 8000 (para que encaje mejor a la traducción original). Para realizar esto procederemos a crear una rama mediante el siguiente comando:

```
git checkout -b feature-overpowered
```

Esto nos creará una nueva rama donde realizaremos nuestros cambios. A continución abriremos el archivo y cambiaremos el valor de la constante y guardemos nuestros cambios en el sistema de archvios.

```java
public class SayayinDTO {
  private String nombre;
  private Integer nivelPoder;
  private static final Integer OVER_POWERED = 8000;
...
```

Añadimos nuestro cambios al stage como vimos en ejercicios pasados, primero revisando el status para asegurarnos que detete el cambio realizado y luego mediante el comando add:

```
git status
git add .
```

Hagamos un commit para guardar los cambios realizados en este branch:

```
git commit -m "Feature - Mejorando la traduccion de la clase SayayinDTO"
```

## La magia de GIT

Git nos permite cambiar el HEAD al que apunta nuestro repositorio en cualquier momento, permitiendonos navegar entre diferentes momentos del código como si de una máquina del tiempo se tratase.

* Comencemos revisando la clase ```SayayinDTO``` que debe tener el cambio que realizamos en el paso anterior.
* Con el comando gitlog podemos ver los cambios que se han realizado en esta rama.

```
git log
```

* Listemos los branches existentes en nuestro repositorio local mediante el comando git branch

```
git branch -av
```

* Volvamos a la rama master mediante el comando checkout para explorar el contenido actual de la rama:

```
git checkout master
```

* Si revisamos la clase ```SayayinDTO``` nos daremos cuenta que nuestro cambio del parámetro no se ha realizado allá ya que sólo afectamos la rama ```feature-overpowered``` con nuestro commit.
* Hagamos nuevamente checkout a la rama ```feature-overpowered``` para percatarnos que nuestros cambios siguen allí:

```
git checkout feature-overpowered
```

* Volvamos una vez más a master y eliminemos la rama feature-overpowered ya que el arreglo de traducción nunca se dará para manetener el meme:

```
git checkout master
git branch -D feature-overpowered
```
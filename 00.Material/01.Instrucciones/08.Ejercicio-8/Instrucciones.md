# Ejercicio 8
Comandos que utilizaremos: [Manual](Manual.md)

## Generemos conflictos
Los conflictos suceden cuando se tocan partes de un mismo archivo por 2 commits diferentes. Al tratar de fusionar estas dos ramas git no sabrá cual versión del archivo es la definitiva generará un conflicto y te pedirá que lo soluciones. Veamos un ejemplo:

* Vuelve, dado el caso de que no estés allí, a la rama "master" del repositorio

```
git checkout master
```

* A continuación crearemos dos ramas a partir de master

```
git checkout -b rama1
git checkout master
git checkout -b rama2
```

* Estando en la rama2 editaremos nuestra clase ```SayayinDTO``` y cambiaremos un poco la funsión "generarMensajePoder". En mi caso he añadido un parametro "nombre" a la función para simplemente concatenar el mismo:

```java
public String generarMensajePoder(String nombre) {
    if (null != nivelPoder && OVER_POWERED.compareTo(nivelPoder) > 0) {
      return nombre + " It's over 9000";
    } else {
      return nombre + "Low power";
    }
  }
```

* Hacemos git add y git commit de estos cambios y nos cambiamos a la rama1

```
git add . 
git commit -m "Generemos un conflicto, rama 2"
git checkout rama1
```

* Este desarrollador pensó que era mucho mejor idea llamar a su variable "name" en lugar de nombre por lo que hace lo mismo pero con el nombre de la variable diferente

```java
public String generarMensajePoder(String name) {
    if (null != nivelPoder && OVER_POWERED.compareTo(nivelPoder) > 0) {
      return name + " It's over 9000";
    } else {
      return name + "Low power";
    }
  }
```

* Hacemos el respectivo git add / commit

```
git add . 
git commit -m "Generemos un conflicto, rama 1"
```

* Esto nos dejará con 2 versiones de la clase diferentes entre cada rama. Ahora trataremos de mergearla:

```
git merge rama2
```

Aparecerá un mensaje como este, dejando la rama en un estado parecido a ```(rama1|MERGING)```

```
CONFLICT (content): Merge conflict in SayayinDTO.java
Automatic merge failed; fix conflicts and then commit the result.
```

* Resolveremos el conflicto escribiendo git mergetool

```
git mergetool
```

* Elegimos en la interfaz gráfica que versión del código se quedará colocando las lineas en el documento del centro y guardando el mismo. Adicionalmente creará un archivo llamado ```SayayinDTO.java.orig``` que guarda la versión anterior del archivo dado el caso hayamos resolvido el coflicto mal podamos restaurarlo. Si por el contrario ya no es necesario puedes borrarlo.

* Al resolver el conflicto git te dejará el los archivos en el stage de manera automática. Solo basta con escribir git commit para que se te abra el editor vi del commit (recuerda presionar esc ":" y luego wq) para que el commit se de y quede la versión del código que elegiste.